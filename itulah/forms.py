from django import forms
from itulah import models
import datetime

##from django.forms import SelectDateWidget


#years = (x for x in range(2019, 2021))


class ScheduleForm(forms.Form):
    nama    = forms.CharField(
            label       = 'nama',
            max_length  = 50,
            required    = True,
            widget      = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder':'gabut'})    
           
        )
    tempat  = forms.CharField(
            label       = 'tempat',
            max_length  = 50,
            required    = True,
            widget      = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder':'gabut'})    
           
        )
    desc    = forms.CharField(
            label       = 'Desc',
            max_length  = 50,
            required    = True,
            widget      = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder':'gabut'})    
            
        )

    date    = forms.DateField(
            initial     = datetime.date.today(),
            label       = 'Date',
            required    = True,
            widget      = forms.SelectDateWidget(attrs = {'class' : 'form-control-sm'})
        )
    
    time    = forms.TimeField(
            label = 'Time',
            widget = forms.TimeInput(format = '%H:%M', attrs={'class' : 'form-control form-control-sm', 'placeholder' : '00:00'}),
        )


class Meta :
    model   = models.Schedule
    fields  = ('nama','tempat','desc','date','time')
